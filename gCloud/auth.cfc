// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
// --- gCloud authentication file
// --- 
// --- Autor: @rsbegue
// --- Data:  15/03/2018
// --- package: 	gcloud
// --- require: 	JWT.*
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
component output="true" accessors="true" {

	property name="jsonFile" 		type="string"  default="credentials.json";
	property name="tokenExpiration" type="numeric" default="600";

	this.GOOGLE_API_AOUTH2_TOKEN = "https://www.googleapis.com/oauth2/v4/token";
	
	public void function init(
		string jsonFile,
		numeric tokenExpiration
		){

		if(structKeyExists(arguments, 'jsonFile') and len(arguments.jsonFile)){
			setJsonFile( arguments.jsonFile );
		}
		if(structKeyExists(arguments, 'tokenExpiration') and len(arguments.tokenExpiration)){
			setTokenExpiration( arguments.tokenExpiration );
		}

	}

	private string function makeSignature(){

		var pathJson = expandPath('./gCloud/');
		var datetime = dateDiff('s', createDateTime(1970, 1, 1, 0, 0, 0), dateAdd('s', GetTimeZoneInfo().utcTotalOffset, now()));

		if(!fileExists(pathJson & getjsonFile())){
			throw(
				type = "gCloud.auth.makeSignature",
				message = "The JSON file not exists."
			);
		}

		var credentials = fileRead( pathJson & getjsonFile() );

		if( ! len(local.credentials) ){
			throw(
				type = "gCloud.auth.makeSignature",
				message = "The JSON fileis invalid."
			);
		}

		if( isJSON(local.credentials) ){
			credentials = deserializeJSON(local.credentials);
		}

		var payload = {
			'iss' 	= credentials.client_email,
			'scope' = "https://www.googleapis.com/auth/devstorage.full_control https://www.googleapis.com/auth/devstorage.read_write",
			'aud' 	= "https://www.googleapis.com/oauth2/v4/token",
			'exp' 	= datetime+getTokenExpiration(),
			'iat' 	= datetime
		};

		var signature = new JWT.client.JsonWebTokensClient(
			new JWT.encode.JsonEncoder(),
			new JWT.encode.Base64urlEncoder(),
			new JWT.sign.RSASigner(
				algorithm = "SHA256withRSA",
				privateKey = credentials.private_key
			)
		).encode( payload );

		return signature;
	}

	private string function makeToken() { 
		
		var httpAccess = new http();
	    httpAccess.setMethod("post");
	    httpAccess.setUrl( this.GOOGLE_API_AOUTH2_TOKEN );
		httpAccess.addParam(type="formfield", name="grant_type", value="urn:ietf:params:oauth:grant-type:jwt-bearer");
		httpAccess.addParam(type="formfield", name="assertion",  value=makeSignature());

		var resultCall = httpAccess.send().getPrefix();
		if(resultCall.statusCode NEQ "200 OK"){
	    	throw(
				type 	= "gCloud.auth.getToken :: #deserializeJSON(resultCall.Filecontent).error#",
				message = deserializeJSON(resultCall.Filecontent).error_description
			);
	    }

	    var resultAccess = resultCall.Filecontent;
	    if( isJSON(resultAccess) ){
	    	resultAccess = deserializeJSON(resultAccess);
	    }

	    if( NOT structKeyExists(resultAccess, 'access_token') OR (structKeyExists(resultAccess, 'access_token') AND not len(resultAccess.access_token)) ){
	    	throw(
				type 	= "gCloud.auth.getToken :: Token error",
				message = "Invalid Token Request"
			);
	    }

	    resultAccess.dtResponse    = now();
	    resultAccess.dtExpiraToken = dateAdd('s', resultAccess.expires_in-5, now());

	    session.gCloud = resultAccess;

		return session.gCloud.access_token;
	}

	public string function getToken(){
		if( structKeyExists(session, 'gcloud') AND structKeyExists(session.gcloud, 'dtExpiraToken') AND len(session.gcloud.dtExpiraToken) AND dateDiff('s', session.gcloud.dtExpiraToken, now()) LT 0 ){
			return session.gcloud.access_token;
		}

		return makeToken();
	}

}