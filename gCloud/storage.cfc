// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
// --- gCloud API storage file
// --- 
// --- Autor: @rsbegue
// --- Data:  15/03/2018
// --- package: 	gcloud
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
component output="true" accessors="true" {

	property name="GCLOUD_BUCKET_NEARLINE"  type="string" default="img-nl";
	property name="GCLOUD_BUCKET_COLDLINE"  type="string" default="img-cl";
	property name="GCLOUD_PROJECT" 			type="string" default="my-project";
	property name="GCLOUD_API_ENDPOINT" 	type="string" default="https://www.googleapis.com/storage/v1/b";
	property name="GCLOUD_API_UPLOAD" 		type="string" default="https://www.googleapis.com/upload/storage/v1/b";

	public any function init(){
		this.auth = new auth();
		return ( this );
	}

	//GET PATH BUCKET
	remote string function getMediaPathRegulacao(required string reference){
		var resultpath = "";

		switch(lCase(arguments.reference))
		{
			case "local":
				resultpath = "//#cgi.SERVER_NAME#/media/";
				break;
			case "nearline":
				resultpath = "http://storage.googleapis.com/#getGCLOUD_BUCKET_NEARLINE()#/";
				break;
			case "coldline":
				resultpath = "http://storage.googleapis.com/#getGCLOUD_BUCKET_COLDLINE()#/";
				break;
			default:
				resultpath = "//#cgi.SERVER_NAME#/media/";
				break;
		}

		return resultpath;
	}

	//LIST BUCKETS
	public struct function getBuckets(required string project){

		var token = this.auth.getToken();

		var httpAccess = new http();
	    httpAccess.setMethod("get");
	    httpAccess.setUrl( getGCLOUD_API_ENDPOINT() );
		httpAccess.addParam(type="header", 	name="Authorization", 	value="Bearer #token#");
		httpAccess.addParam(type="url", 	name="project",  		value=arguments.project);

		var resultCall = httpAccess.send().getPrefix();
		if(resultCall.statusCode NEQ "200 OK"){
	    	throw(
				type 	= "gCloud.storage.getBuckets :: #deserializeJSON(resultCall.fileContent).error#",
				message = deserializeJSON(resultCall.fileContent).error_description
			);
	    }

		return deserializeJSON(resultCall.fileContent);
	}

	//LIST OBJECTS IN BUCKETS
	public struct function getObjects(required string bucket, string prefix){

		var token = this.auth.getToken();

		var httpAccess = new http();
	    httpAccess.setMethod("get");
	    httpAccess.setUrl( getGCLOUD_API_ENDPOINT() & '/' & arguments.bucket &'/o' );
		httpAccess.addParam(type="header", 	name="Authorization", 	value="Bearer #token#");
		if( structKeyExists(arguments, 'prefix') and len(arguments.prefix) ){
			httpAccess.addParam(type="url", name="project", value=arguments.prefix);
		}

		var resultCall = httpAccess.send().getPrefix();
		if(resultCall.statusCode NEQ "200 OK"){
	    	throw(
				type 	= "gCloud.storage.getObjects :: #deserializeJSON(resultCall.fileContent).error#",
				message = deserializeJSON(resultCall.fileContent).error_description
			);
	    }

		return deserializeJSON(resultCall.fileContent);
	}

	//INSERT OBJECT (UPLOAD) IN BUCKET
	public struct function createObject(
		required string bucket,
		required string localPath,
		required string mediaPath,
		required string filename
	){

		var token 		  = this.auth.getToken();
		var localFullPath = arguments.localPath & '\' & arguments.filename;
		var localImage    = isImageFile(localFullPath) ? imageRead(localFullPath) : fileRead(localFullPath);

		var httpAccess = new http();
	    httpAccess.setMethod("post");
	    httpAccess.setUrl( getGCLOUD_API_UPLOAD() & '/' & arguments.bucket &'/o' );
		httpAccess.addParam(type="header", 	name="Authorization", 	value="Bearer #token#");
		httpAccess.addParam(type="header", 	name="Content-Type", 	value="#fileGetMimeType(localFullPath)#");
		httpAccess.addParam(type="url", 	name="uploadType", 		value="media");
		httpAccess.addParam(type="url", 	name="name", 			value="#arguments.mediaPath#/#arguments.filename#");
		httpAccess.addParam(type="body", 	value="#toBinary( toBase64(localImage) )#");
		
		var resultCall = httpAccess.send().getPrefix();
		if(resultCall.statusCode NEQ "200 OK"){
	    	throw(
				type 	= "gCloud.storage.createObject :: #deserializeJSON(resultCall.fileContent).error#",
				message = deserializeJSON(resultCall.fileContent).error_description
			);
	    }

		return deserializeJSON(resultCall.fileContent);
	}

}